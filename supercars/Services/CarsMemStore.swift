//
//  CarsMemStore.swift
//  supercars
//
//  Created by Arlen Ricardo Pereira on 07/04/18.
//  Copyright © 2018 Arlen Ricardo Pereira. All rights reserved.
//

import Foundation

class CarsMemStore: CarsStoreProtocol
{
    // MARK: Data
    static var imageAstonMartinDB11_0 = "https://firebasestorage.googleapis.com/v0/b/mostwantedcars-7942b.appspot.com/o/AustonMartin%2FDB11%2FAuston%20Matin%20DB11%20-%2001.jpg?alt=media&token=1ff99f3a-ef8c-4701-88b3-07e2b2bd310d"
    static var imageAstonMartinDB11_1 = "https://firebasestorage.googleapis.com/v0/b/mostwantedcars-7942b.appspot.com/o/AustonMartin%2FDB11%2FAuston%20Matin%20DB11%20-%2002.jpg?alt=media&token=839d8e5f-7e57-46b5-8bab-76bbbb0b7a8a"
    static var imageAstonMartinDB11_2 = "https://firebasestorage.googleapis.com/v0/b/mostwantedcars-7942b.appspot.com/o/AustonMartin%2FDB11%2FAuston%20Matin%20DB11%20-%2003.jpg?alt=media&token=cae98b20-9053-4b8d-8499-562eb99d82f7"
    static var imageAstonMartinDB11_3 = "https://firebasestorage.googleapis.com/v0/b/mostwantedcars-7942b.appspot.com/o/AustonMartin%2FDB11%2FAuston%20Matin%20DB11%20-%2004.jpg?alt=media&token=d32b2006-079f-4bde-a773-b6efcea0d8db"
    static var imageAstonMartinDB11_4 = "https://firebasestorage.googleapis.com/v0/b/mostwantedcars-7942b.appspot.com/o/AustonMartin%2FDB11%2FAuston%20Matin%20DB11%20-%2005.jpg?alt=media&token=65683901-cac9-4519-8c84-954958926438"
    static var imageAstonMartinDB11_5 = "https://firebasestorage.googleapis.com/v0/b/mostwantedcars-7942b.appspot.com/o/AustonMartin%2FDB11%2FAuston%20Matin%20DB11%20-%2006.jpg?alt=media&token=c6e77925-45c9-4a58-ade5-0dce1e4e9732"
    static var imageAstonMartinDB11_6 = "https://firebasestorage.googleapis.com/v0/b/mostwantedcars-7942b.appspot.com/o/AustonMartin%2FDB11%2FAuston%20Matin%20DB11%20-%2007.jpg?alt=media&token=e73dd2c9-56f5-4526-b7bd-08816341d8c2"
    static var imageAstonMartinDB11_7 = "https://firebasestorage.googleapis.com/v0/b/mostwantedcars-7942b.appspot.com/o/AustonMartin%2FDB11%2FAuston%20Matin%20DB11%20-%2008.jpg?alt=media&token=4cc0bb9e-f9dd-4831-95ce-6538a1d69aa1"
    static var imageAstonMartinDB11_8 = "https://firebasestorage.googleapis.com/v0/b/mostwantedcars-7942b.appspot.com/o/AustonMartin%2FDB11%2FAuston%20Matin%20DB11%20-%2009.jpg?alt=media&token=0dd3d045-1e0d-4f3b-99fb-e8fd1912cdc8"
    static var imageAstonMartinDB11_9 = "https://firebasestorage.googleapis.com/v0/b/mostwantedcars-7942b.appspot.com/o/AustonMartin%2FDB11%2FAuston%20Matin%20DB11%20-%2010.jpg?alt=media&token=82952291-10ad-4477-8736-77dc253e5117"
    static var imageAstonMartinDB11_10 = "https://firebasestorage.googleapis.com/v0/b/mostwantedcars-7942b.appspot.com/o/AustonMartin%2FDB11%2FAuston%20Matin%20DB11%20-%2011.jpg?alt=media&token=fa7e4154-b094-469b-8477-f6bd1a0c5b53"
    static var imageAstonMartinDB11_11 = "https://firebasestorage.googleapis.com/v0/b/mostwantedcars-7942b.appspot.com/o/AustonMartin%2FDB11%2FAuston%20Matin%20DB11%20-%2012.jpg?alt=media&token=726fb168-0041-42e5-bf30-9fa74daeac52"
    static var imageAstonMartinDB11_12 = "https://firebasestorage.googleapis.com/v0/b/mostwantedcars-7942b.appspot.com/o/AustonMartin%2FDB11%2FAuston%20Matin%20DB11%20-%2020.jpg?alt=media&token=342ea792-2f7f-4ea9-9497-f3d129ed5441"
    static var imageAstonMartinDB11_13 = "https://firebasestorage.googleapis.com/v0/b/mostwantedcars-7942b.appspot.com/o/AustonMartin%2FDB11%2FAuston%20Matin%20DB11%20-%2021.jpg?alt=media&token=40897535-0032-429b-a8b4-a98390bf2f42"
    static var imageAstonMartinDB11_14 = "https://firebasestorage.googleapis.com/v0/b/mostwantedcars-7942b.appspot.com/o/AustonMartin%2FDB11%2FAuston%20Matin%20DB11%20-%2022.jpg?alt=media&token=a8910402-b8ec-4956-b499-69f78fabca57"
    static var imageAstonMartinDB11_15 = "https://firebasestorage.googleapis.com/v0/b/mostwantedcars-7942b.appspot.com/o/AustonMartin%2FDB11%2FAuston%20Matin%20DB11%20-%2023.jpg?alt=media&token=c7043a61-5a7f-4f80-bb31-cf5910b91f58"
    
    static var imagesAstonMrtinDB11 = [imageAstonMartinDB11_0, imageAstonMartinDB11_1, imageAstonMartinDB11_2, imageAstonMartinDB11_3, imageAstonMartinDB11_4, imageAstonMartinDB11_5, imageAstonMartinDB11_6, imageAstonMartinDB11_7, imageAstonMartinDB11_8, imageAstonMartinDB11_9, imageAstonMartinDB11_10, imageAstonMartinDB11_11, imageAstonMartinDB11_12, imageAstonMartinDB11_13, imageAstonMartinDB11_14, imageAstonMartinDB11_15]
    
    
    static var astonMartinCompany = Manufacturer.init(idManuf:"0",
                                                      fullAddress: "Banbury Road - Gaydon, Warwick - CV35 0DB - United Kingdom",
                                                      fullName: "Aston Martin Lagonda Ltd.",
                                                      image: "https://firebasestorage.googleapis.com/v0/b/mostwantedcars-7942b.appspot.com/o/AustonMartin%2FAston-Martin_Headquarter.jpg?alt=media&token=f254ed8a-6def-418e-afcc-1bccdceb01f1",
                                                      latitude: 52.195017,
                                                      logo: "https://firebasestorage.googleapis.com/v0/b/mostwantedcars-7942b.appspot.com/o/AustonMartin%2FAston-Martin-Logo_Translucent.png?alt=media&token=d76cf6a0-b731-4376-abf5-3d7931548579",
                                                      longitude: -1.484948,
                                                      profile: "Aston Martin’s state-of-the-art headquarters in Gaydon, Warwickshire is built upon the passion, skill and creativity of the people who dedicate their working lives to the most iconic brand in the world. Every intricate stitch, beautifully expressed line, and the stirring noise of our engines, is a manifestation of the desire and collective spirit to create the most beautiful cars: the soul of Aston Martin made real.")
    
    static var car1 = Car.init(idCar: "0",
                               acceleration: "3.9",
                               image: "https://firebasestorage.googleapis.com/v0/b/mostwantedcars-7942b.appspot.com/o/AustonMartin%2FDB11%2FAuston%20Matin%20DB11%20-%20cover.jpg?alt=media&token=6e972cac-eb3b-490e-b157-7f7af523fa98",
                               model: "Aston Martin DB11",
                               coEmission: "This vehicle complies with energy efficiency class G",
                               cyclinders: "5,200cc",
                               date: 1483590668,
                               isNewCar: true,
                               maxSpeed: "200 mph (322 km/h)",
                               motor: "All-alloy quad overhead cam, 48 valve, 5.2l bi-turbo, V12 with stop/start cylinder de-activation",
                               power: "608ps (447kW) 600hp @ 6500 rpm",
                               tag: "Auston Martin, DB11",
                               torque: "700Nm (516lb-ft) @ 1,500rpm",
                               videoURL: "l_s_RQjEifY",
                               site: "http://db11.astonmartin.com/en",
                               manufacturer: astonMartinCompany,
                               imagesCar: imagesAstonMrtinDB11)
    
    static var cars = [car1, car1, car1, car1, car1, car1, car1, car1, car1]
    
    // MARK: - CRUD operations - Inner closure
    
    func fetchCars(completionHandler: @escaping (() throws -> [Car]) -> Void) {
        completionHandler { return type(of: self).cars }
    }
}
