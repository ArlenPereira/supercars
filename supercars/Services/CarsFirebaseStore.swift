//
//  CarsFirebaseStore.swift
//  supercars
//
//  Created by Arlen Ricardo Pereira on 23/04/18.
//  Copyright © 2018 Arlen Ricardo Pereira. All rights reserved.
//

import UIKit
import FirebaseDatabase

class CarsFirebaseStore: CarsStoreProtocol
{
    // MARK: - CRUD operations - Inner closure
    
    func fetchCars(completionHandler: @escaping (() throws -> [Car]) -> Void) {
        var carsList = [Car]()
        
        var reference: DatabaseReference!
        reference = Database.database().reference(fromURL: "https://mostwantedcars-7942b.firebaseio.com/")
        reference.child("superCars/carsInfo").queryOrdered(byChild: "isActived").queryEqual(toValue: true).observe(.value, with: { (snapshot) in
            carsList.removeAll()
            if let snapshots = snapshot.value as? [String: [String: AnyObject]] {
                for (carKey, carValues) in snapshots {
                    
                    let key = carKey
                    let carAcceleration = carValues["acceleration"] as! String
                    let carImage = carValues["carImage"] as! String
                    let carModel = carValues["carModel"] as! String
                    let carCOEmisson = carValues["coEmission"] as! String
                    let carCylinders = carValues["cylinders"] as! String
                    let carDate = carValues["date"] as! Int
                    let carIsNewCar = carValues["isNewCar"] as! Bool
                    let carMaxSpeed = carValues["carImage"] as! String
                    let carMotor = carValues["motor"] as! String
                    let carPower = carValues["power"] as! String
                    let carTag = carValues["tag"] as! String
                    let carTorque = carValues["torque"] as! String
                    let carVideoURL = carValues["videoURL"] as! String
                    let carSite = carValues["site"] as! String
                    let manufCode = carValues["manufCode"] as! String
                    
                    var photosCar = [String]()
                    
                    reference.child("superCars/carsPhotos").child(key).observe(.value, with: { (snapshotPhotos) in
                        
                        if let snapshotsPhotos = snapshotPhotos.children.allObjects as? [DataSnapshot] {
                            for snap in snapshotsPhotos {
                                let carPhoto = snap.value as! String
                                photosCar.append(carPhoto)
                            }
                        }
                        
                        var manufacturerInfo: Manufacturer? = nil
                        
                        reference.child("manufacture").queryEqual(toValue: nil, childKey: manufCode).observe(.value, with: { (snapshotManuf) in
                            
                            if let snapshotsManuf = snapshotManuf.value as? [String: [String: AnyObject]] {
                                for (manufKey, manufValue) in snapshotsManuf {
                                    let manufAddress = manufValue["fullAddress"] as! String
                                    let manufName = manufValue["fullName"] as! String
                                    let manufImage = manufValue["image"] as! String
                                    let manufLatitude = manufValue["latitude"] as! Double
                                    let manufLogo = manufValue["logoManufacturer"] as! String
                                    let manufLongitude = manufValue["longitude"] as! Double
                                    let manufProfile = manufValue["profile"] as! String
                                    
                                    manufacturerInfo = Manufacturer(idManuf: manufKey, fullAddress: manufAddress, fullName: manufName, image: manufImage, latitude: manufLatitude, logo: manufLogo, longitude: manufLongitude, profile: manufProfile)
                                }
                            }
                            
                            let cars = Car(idCar: carKey, acceleration: carAcceleration, image: carImage, model: carModel, coEmission: carCOEmisson, cyclinders: carCylinders, date: carDate, isNewCar: carIsNewCar, maxSpeed: carMaxSpeed, motor: carMotor, power: carPower, tag: carTag, torque: carTorque, videoURL: carVideoURL, site: carSite, manufacturer: manufacturerInfo!, imagesCar: photosCar)
                            
                            carsList.append(cars)
                            completionHandler { return carsList }
                        })
                    })
                }
            }
            
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
