//
//  DetailCarViewController.swift
//  supercars
//
//  Created by Arlen Ricardo Pereira on 08/04/18.
//  Copyright (c) 2018 Arlen Ricardo Pereira. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import youtube_ios_player_helper
import MMBannerLayout

protocol DetailCarDisplayLogic: class
{
  func displayCarInfo(viewModel: DetailCar.GetCar.ViewModel)
}

class DetailCarViewController: UIViewController, DetailCarDisplayLogic, YTPlayerViewDelegate
{
  var interactor: DetailCarBusinessLogic?
  var router: (NSObjectProtocol & DetailCarRoutingLogic & DetailCarDataPassing)?
    var imagesCar: [String]?
    let cellId = "imageCell"
    
  // MARK: Interfaces
    
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var motorLabel: UILabel!
    @IBOutlet weak var accelerationLabel: UILabel!
    @IBOutlet weak var maxSpeedLabel: UILabel!
    @IBOutlet weak var powerLabel: UILabel!
    @IBOutlet weak var torqueLabel: UILabel!
    @IBOutlet weak var cyclindresLabel: UILabel!
    @IBOutlet weak var coEmissionLabel: UILabel!
    @IBOutlet weak var manufacturerImageView: UIImageView!
    @IBOutlet weak var manufacturerLabel: UILabel!
    @IBOutlet weak var youTubeView: YTPlayerView!
    @IBOutlet weak var carPhotosCollectionView: UICollectionView!
    
  // MARK: Object lifecycle
    
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
  {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder)
  {
    super.init(coder: aDecoder)
    setup()
  }
  
  // MARK: Setup
  
  private func setup()
  {
    let viewController = self
    let interactor = DetailCarInteractor()
    let presenter = DetailCarPresenter()
    let router = DetailCarRouter()
    viewController.interactor = interactor
    viewController.router = router
    interactor.presenter = presenter
    presenter.viewController = viewController
    router.viewController = viewController
    router.dataStore = interactor
  }
  
  // MARK: Routing
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?)
  {
    if let scene = segue.identifier {
      let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
      if let router = router, router.responds(to: selector) {
        router.perform(selector, with: segue)
      }
    }
  }
  
  // MARK: View lifecycle
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
    youTubeView.delegate = self
    if let layout = carPhotosCollectionView.collectionViewLayout as? MMBannerLayout {
        layout.itemSpace = 0
        layout.itemSize = self.carPhotosCollectionView.frame.insetBy(dx: 30, dy: 30).size
        layout.minimuAlpha = 0.4
        layout.setInfinite(isInfinite: false, completed: nil)
        layout.angle = 40.0
    }
    fetchCar()
  }
  
  // MARK: Get car
  
  func fetchCar()
  {
    let request = DetailCar.GetCar.Request()
    interactor?.requestGetCar(request: request)
  }
  
  func displayCarInfo(viewModel: DetailCar.GetCar.ViewModel)
  {
    let displayedDetailCar = viewModel.displayedDetailCar
    imagesCar = displayedDetailCar.imagesCar
    modelLabel.text = displayedDetailCar.model
    motorLabel.text = displayedDetailCar.motor
    accelerationLabel.text = "\(displayedDetailCar.acceleration) sec."
    maxSpeedLabel.text = displayedDetailCar.maxSpeed
    powerLabel.text = displayedDetailCar.power
    torqueLabel.text = displayedDetailCar.torque
    cyclindresLabel.text = displayedDetailCar.cyclinders
    coEmissionLabel.text = displayedDetailCar.coEmission
    manufacturerLabel.text = displayedDetailCar.manufacturer.fullName
    
    carImageView.sd_setShowActivityIndicatorView(true)
    carImageView.sd_setIndicatorStyle(.white)
    carImageView.sd_setImage(with: URL(string: displayedDetailCar.carImage), placeholderImage: nil, options: [.continueInBackground])
    
    manufacturerImageView.sd_setShowActivityIndicatorView(true)
    manufacturerImageView.sd_setIndicatorStyle(.white)
    manufacturerImageView.sd_setImage(with: URL(string: displayedDetailCar.manufacturer.logo), placeholderImage: nil, options: [.continueInBackground])
    
    // Configuration parametes to play Youtube
    let playerVar = ["playsinline": 0]
    youTubeView.load(withVideoId: displayedDetailCar.videoURL, playerVars: playerVar)
  }
    
    @IBAction func manufacturerButton(_ sender: UIButton) {
        performSegue(withIdentifier: "DetailCarManufacturer", sender: sender)
    }
    
    @IBAction func siteButton(_ sender: UIButton) {
        performSegue(withIdentifier: "DetailCarSite", sender: sender)
    }
}

extension DetailCarViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesCar!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! DetailCarPhotosViewCell
        let displayedPhotosCar = imagesCar![indexPath.row]
        cell.photoImageView.sd_setShowActivityIndicatorView(true)
        cell.photoImageView.sd_setIndicatorStyle(.white)
        cell.photoImageView.sd_setImage(with: URL(string: displayedPhotosCar), placeholderImage: nil, options: [.continueInBackground])
        return cell
    }
}

class DetailCarPhotosViewCell: UICollectionViewCell {
    // MARK: - Interface Cell
    @IBOutlet weak var photoImageView: UIImageView!
    
    // MARK: View lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
