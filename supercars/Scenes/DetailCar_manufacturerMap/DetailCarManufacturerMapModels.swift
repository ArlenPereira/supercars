//
//  DetailCarManufacturerMapModels.swift
//  supercars
//
//  Created by Arlen Ricardo Pereira on 14/04/18.
//  Copyright (c) 2018 Arlen Ricardo Pereira. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import MapKit

enum DetailCarManufacturerMap
{
  // MARK: Use cases
  
  enum GetLocation
  {
    struct Request
    {
    }
    struct Response
    {
        var location: Manufacturer
    }
    struct ViewModel
    {
        struct DisplayedLocation {
            var fullName: String
            var fullAddress: String
            var image: String
            var latitude: Double
            var logo: String
            var longitude: Double
        }
        
        var displayedLocation: DisplayedLocation
    }
  }
}

class AnnotationInfo: NSObject, MKAnnotation {
    let logo: String?
    let title: String?
    let coordinate: CLLocationCoordinate2D
    
    init(logo: String, title: String, coordinate: CLLocationCoordinate2D) {
        self.logo = logo
        self.title = title
        self.coordinate = coordinate
        
        super.init()
    }
}
