//
//  DetailCarManufacturerMapInteractor.swift
//  supercars
//
//  Created by Arlen Ricardo Pereira on 14/04/18.
//  Copyright (c) 2018 Arlen Ricardo Pereira. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol DetailCarManufacturerMapBusinessLogic
{
  func requestGetLocation(request: DetailCarManufacturerMap.GetLocation.Request)
}

protocol DetailCarManufacturerMapDataStore
{
    var location: Manufacturer! { get set }
}

class DetailCarManufacturerMapInteractor: DetailCarManufacturerMapBusinessLogic, DetailCarManufacturerMapDataStore
{
  var presenter: DetailCarManufacturerMapPresentationLogic?
  var worker: DetailCarManufacturerMapWorker?
  var location: Manufacturer!
  
  // MARK: Do something
  
  func requestGetLocation(request: DetailCarManufacturerMap.GetLocation.Request)
  {
    let response = DetailCarManufacturerMap.GetLocation.Response(location: location)
    presenter?.requestGetLocation(response: response)
  }
}
