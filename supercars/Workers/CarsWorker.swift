//
//  CarsWorker.swift
//  supercars
//
//  Created by Arlen Ricardo Pereira on 07/04/18.
//  Copyright © 2018 Arlen Ricardo Pereira. All rights reserved.
//

import Foundation

class CarsWorker
{
    var carsStore: CarsStoreProtocol
    
    init(carsStore: CarsStoreProtocol)
    {
        self.carsStore = carsStore
    }
    
    func fetchCars(complationHandler: @escaping ([Car]) -> Void)
    {
        carsStore.fetchCars { (cars: () throws -> [Car]) -> Void in
            do {
                let cars = try cars()
                DispatchQueue.main.async {
                    complationHandler(cars)
                }
            } catch {
                DispatchQueue.main.async {
                    complationHandler([])
                }
            }
        }
    }
}

protocol CarsStoreProtocol {
    
    // MARK: - CRUD operations - Inner closure
    
    func fetchCars(completionHandler: @escaping (() throws -> [Car]) -> Void)
}
