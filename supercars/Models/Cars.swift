//
//  Cars.swift
//  supercars
//
//  Created by Arlen Ricardo Pereira on 06/04/18.
//  Copyright © 2018 Arlen Ricardo Pereira. All rights reserved.
//

import Foundation

// MARK: Car info
struct Car: Equatable {
    var idCar: String
    var acceleration: String
    var image: String
    var model: String
    var coEmission: String
    var cyclinders: String
    var date: Int
    var isNewCar: Bool
    var maxSpeed: String
    var motor: String
    var power: String
    var tag: String
    var torque: String
    var videoURL: String
    var site: String
    var manufacturer: Manufacturer
    var imagesCar: [String]
}

func ==(lhs: Car, rhs: Car) -> Bool {
    return lhs.idCar == rhs.idCar
    && lhs.acceleration == rhs.acceleration
    && lhs.image == rhs.image
    && lhs.model == rhs.model
    && lhs.coEmission == rhs.coEmission
    && lhs.cyclinders == rhs.cyclinders
    && lhs.date == rhs.date
    && lhs.isNewCar == rhs.isNewCar
    && lhs.maxSpeed == rhs.maxSpeed
    && lhs.motor == rhs.motor
    && lhs.power == rhs.power
    && lhs.tag == rhs.tag
    && lhs.torque == rhs.torque
    && lhs.videoURL == rhs.videoURL
    && lhs.site == rhs.site
    && lhs.manufacturer == rhs.manufacturer
}

// MARK: Manufacture info
struct Manufacturer {
    var idManuf: String
    var fullAddress: String
    var fullName: String
    var image: String
    var latitude: Double
    var logo: String
    var longitude: Double
    var profile: String
}

func == (lhs: Manufacturer, rhs: Manufacturer) -> Bool {
    return lhs.idManuf == rhs.idManuf
    && lhs.fullAddress == rhs.fullAddress
    && lhs.fullName == rhs.fullName
    && lhs.image == rhs.image
    && lhs.latitude == rhs.latitude
    && lhs.logo == rhs.logo
    && lhs.longitude == rhs.longitude
    && lhs.profile == rhs.profile
}

// MARK: Favorite info
struct Favorite {
    var idCar: String
    var isFavorite: Bool
}

func == (lhs: Favorite, rhs: Favorite) -> Bool {
    return lhs.idCar == rhs.idCar
    && lhs.isFavorite == rhs.isFavorite
}
